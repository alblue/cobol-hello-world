all: hello

hello: hello.cbl
	cobc -x -free hello.cbl

run: hello
	./hello

clean:
	rm -f hello
