Example COBOL with GitPod
=========================

This demonstrates how to compile and run a COBOL program
using GitPod to host a workspace. The [GitPod YAML](.gitpod.yml)
contains a list of extensions that provide syntax highlighting
and others, based on the IBM Z Open Editor which can be
found at https://marketplace.visualstudio.com/items?itemName=IBM.zopeneditor
and its dependency, the Zowe extension as well.
https://marketplace.visualstudio.com/items?itemName=Zowe.vscode-extension-for-zowe

Visiting https://gitpod.io/#https://gitlab.com/alblue/cobol-hello-world
will result in a configured GitPod workspace that allows you to edit and
run your first Hello World program in COBOL!

For more information on COBOL see https://en.wikipedia.org/wiki/GnuCOBOL
which is the compiler configured for this project in the [GitPod Dockerfile](.gitpod.Dockerfile).
